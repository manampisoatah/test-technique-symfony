<?php

namespace App\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;
use PhpOffice\PhpSpreadsheet\IOFactory;
use Symfony\Component\HttpFoundation\Request;
use App\Service\FileUploader;
use App\Form\ImportExcelType;
use App\Entity\Vehicule;
use App\Entity\Proprietaire;

class ImportExcelController extends AbstractController
{

    /**
     * @Route("/", name="save")
     */
    public function save(Request $request, FileUploader $file_uploader)
    {
        $form = $this->createForm(ImportExcelType::class);
        $form->handleRequest($request);

        $flashBag = $this->get('session')->getFlashBag();
        $flashBag->get('success');

        if ($form->isSubmitted() && $form->isValid()) {
            $_sFile = $form['upload_file']->getData();
            if ($_sFile) {
                $_sFileName = $file_uploader->upload($_sFile);
                if ( null !== $_sFileName )
                {
                    $_sDirectory = $file_uploader->getTargetDirectory();
                    $_sFullPath = $_sDirectory . '/' . $_sFileName;
                    $spreadsheet = IOFactory::load($_sFullPath);

                    $spreadsheet->getActiveSheet()->removeRow(1);
                    $sheetData = $spreadsheet->getActiveSheet()->toArray(null, true, true, true);
                    $entityManager = $this->getDoctrine()->getManager();
                    foreach ($sheetData as $Row)
                    {
                        // Champs de la table proprietaire
                        $_iNumeroFiche = $Row['D'];
                        $_sLibelleCivilite = $Row['E'];
                        $_sProprietaireActuelVehicule = $Row['F'];
                        $_sNom = $Row['G'];
                        $_sPrenom = $Row['H'];
                        $_sNumeroNomVoie = $Row['I'];
                        $_sComplementAdress = $Row['J'];
                        $_sCodePostal = $Row['K'];
                        $_sVille = $Row['L'];
                        $_iTelephoneDomicile = $Row['M'];
                        $_iTelephonePortable = $Row['N'];
                        $_iTelephoneJob = $Row['O'];
                        $_sEmail = $Row['P'];

                        $_oProprietaireExistant = $entityManager->getRepository(Proprietaire::class)->findOneByNumeroFiche($_iNumeroFiche);
                        if (!$_oProprietaireExistant && !is_null($_iNumeroFiche))
                        {
                            $_oProprietaire = new Proprietaire();
                            $_oProprietaire->setNumeroDeFiche($_iNumeroFiche);
                            $_oProprietaire->setLibelleCivilite($_sLibelleCivilite);
                            $_oProprietaire->setProprietaireActuelDuVehicule($_sProprietaireActuelVehicule);
                            $_oProprietaire->setNom($_sNom);
                            $_oProprietaire->setPrenom($_sPrenom);
                            $_oProprietaire->setNumeroEtNomDeLaVoie($_sNumeroNomVoie);
                            $_oProprietaire->setComplementAdresse1($_sComplementAdress);
                            $_oProprietaire->setCodePostal($_sCodePostal);
                            $_oProprietaire->setVille($_sVille);
                            $_oProprietaire->setTelephoneDomicile($_iTelephoneDomicile);
                            $_oProprietaire->setTelephonePortable($_iTelephonePortable);
                            $_oProprietaire->setTelephoneJob($_iTelephoneJob);
                            $_oProprietaire->setEmail($_sEmail);
                            $entityManager->persist($_oProprietaire);
                            $entityManager->flush();

                            $_oProprietaireExistant = $_oProprietaire;
                        }

                        // Champs de la table vehicule
                        $s_CompteAffaire = $Row['A'];
                        $_sCompteEvenement = $Row['B'];
                        $_sCompteDernierEvenement = $Row['C'];
                        $_dDateMiseCirculation = new \DateTime($Row['Q']);
                        $_dDateAchat = new \DateTime($Row['R']);
                        $_dDateDernierEvenement =  new \DateTime($Row['S']);
                        $_sLibelleMarque = $Row['T'];
                        $_sLibelleModele = $Row['U'];
                        $_sVersion = $Row['V'];
                        $_sVin = $Row['W'];
                        $_sImmatriculation = $Row['X'];
                        $_sTypeProspect = $Row['Y'];
                        $_iKilometrage = $Row['Z'];
                        $_sLibelleEnergie = $Row['AA'];
                        $_sVendeurVn = $Row['AB'];
                        $_sVendeurVo = $Row['AC'];
                        $_sCommentaireFacturation = $Row['AD'];
                        $_sTypeVnVo = $Row['AE'];
                        $_iNumeroDossier = $Row['AF'];
                        $_sIntermediaireVente = $Row['AG'];
                        $_dDateEvenement =  new \DateTime($Row['AH']);
                        $_sOrigineEvenement = $Row['AI'];

                        $_oVehiculeExistant = $entityManager->getRepository(Vehicule::class)->findOneByVin($_sVin);
                        if(!$_oVehiculeExistant && !is_null($_sVin)){
                            $_oVehicule = new Vehicule();
                            $_oVehicule->setProprietaire($_oProprietaireExistant);
                            $_oVehicule->setCompteAffaire($s_CompteAffaire);
                            $_oVehicule->setCompteEvenement($_sCompteEvenement);
                            $_oVehicule->setCompteDernierEvenement($_sCompteDernierEvenement);
                            $_oVehicule->setDateDeMiseEnCirculation($_dDateMiseCirculation);
                            $_oVehicule->setDateAchat($_dDateAchat);
                            $_oVehicule->setDateDernierEvenement($_dDateDernierEvenement);
                            $_oVehicule->setLibelleMarque($_sLibelleMarque);
                            $_oVehicule->setLibelleModele($_sLibelleModele);
                            $_oVehicule->setVersion($_sVersion);
                            $_oVehicule->setVin($_sVin);
                            $_oVehicule->setImmatriculation($_sImmatriculation);
                            $_oVehicule->setTypeDeProspect($_sTypeProspect);
                            $_oVehicule->setKilometrage($_iKilometrage);
                            $_oVehicule->setLibelleEnergie($_sLibelleEnergie);
                            $_oVehicule->setVendeurVn($_sVendeurVn);
                            $_oVehicule->setVendeurVo($_sVendeurVo);
                            $_oVehicule->setCommentaireDeFacture($_sCommentaireFacturation);
                            $_oVehicule->setTypeVnVo($_sTypeVnVo);
                            $_oVehicule->setNumeroDeDossierVn($_iNumeroDossier);
                            $_oVehicule->setIntermediaireDeVente($_sIntermediaireVente);
                            $_oVehicule->setDateEvenement($_dDateEvenement);
                            $_oVehicule->setOrigineEvenement($_sOrigineEvenement);

                            $entityManager->persist($_oVehicule);
                            $entityManager->flush();
                        }

                    }

                    $this->addFlash('success', 'Import Excel terminé avec succès');

                } else {
                    $this->addFlash('error-msg', 'Erreur lors de l\'import du fichier');
                }
            }
        }

        return $this->render('import_excel/save.html.twig', [
            'form' => $form->createView(),
        ]);

    }
}
