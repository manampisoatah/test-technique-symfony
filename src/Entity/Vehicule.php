<?php

namespace App\Entity;

use App\Repository\VehiculeRepository;
use Doctrine\ORM\Mapping as ORM;

/**
 * @ORM\Entity(repositoryClass=VehiculeRepository::class)
 */
class Vehicule
{
    /**
     * @ORM\Id
     * @ORM\GeneratedValue
     * @ORM\Column(type="integer")
     */
    private $id;

    /**
     * @ORM\Column(type="string", length=100, nullable=true)
     */
    private $compteAffaire;

    /**
     * @ORM\Column(type="string", length=100, nullable=true)
     */
    private $compteEvenement;

    /**
     * @ORM\Column(type="string", length=100, nullable=true)
     */
    private $compteDernierEvenement;

    /**
     * @ORM\Column(type="datetime", nullable=true)
     */
    private $dateDeMiseEnCirculation;

    /**
     * @ORM\Column(type="datetime", nullable=true)
     */
    private $dateAchat;

    /**
     * @ORM\Column(type="datetime", nullable=true)
     */
    private $dateDernierEvenement;

    /**
     * @ORM\Column(type="string", length=100, nullable=true)
     */
    private $libelleMarque;

    /**
     * @ORM\Column(type="string", length=100, nullable=true)
     */
    private $libelleModele;

    /**
     * @ORM\Column(type="string", length=100, nullable=true)
     */
    private $version;

    /**
     * @ORM\Column(type="string", length=100)
     */
    private $vin;

    /**
     * @ORM\Column(type="string", length=100, nullable=true)
     */
    private $immatriculation;

    /**
     * @ORM\Column(type="string", length=100, nullable=true)
     */
    private $typeDeProspect;

    /**
     * @ORM\Column(type="integer", nullable=true)
     */
    private $kilometrage;

    /**
     * @ORM\Column(type="string", length=100, nullable=true)
     */
    private $libelleEnergie;

    /**
     * @ORM\Column(type="string", length=100, nullable=true)
     */
    private $vendeurVn;

    /**
     * @ORM\Column(type="string", length=100, nullable=true)
     */
    private $vendeurVo;

    /**
     * @ORM\Column(type="text", nullable=true)
     */
    private $commentaireDeFacture;

    /**
     * @ORM\Column(type="string", length=2, nullable=true)
     */
    private $typeVnVo;

    /**
     * @ORM\Column(type="string", length=100, nullable=true)
     */
    private $numeroDeDossierVn;

    /**
     * @ORM\Column(type="string", length=255, nullable=true)
     */
    private $intermediaireDeVente;

    /**
     * @ORM\Column(type="datetime", nullable=true)
     */
    private $dateEvenement;

    /**
     * @ORM\Column(type="string", length=100, nullable=true)
     */
    private $origineEvenement;

    /**
     * @ORM\ManyToOne(targetEntity="App\Entity\Proprietaire", inversedBy="vehicule")
     */
    private $proprietaire;

    public function getId(): ?int
    {
        return $this->id;
    }

    public function getCompteAffaire(): ?string
    {
        return $this->compteAffaire;
    }

    public function setCompteAffaire(?string $compteAffaire): self
    {
        $this->compteAffaire = $compteAffaire;

        return $this;
    }

    public function getCompteEvenement(): ?string
    {
        return $this->compteEvenement;
    }

    public function setCompteEvenement(?string $compteEvenement): self
    {
        $this->compteEvenement = $compteEvenement;

        return $this;
    }

    public function getCompteDernierEvenement(): ?string
    {
        return $this->compteDernierEvenement;
    }

    public function setCompteDernierEvenement(?string $compteDernierEvenement): self
    {
        $this->compteDernierEvenement = $compteDernierEvenement;

        return $this;
    }

    public function getDateDeMiseEnCirculation(): ?\DateTimeInterface
    {
        return $this->dateDeMiseEnCirculation;
    }

    public function setDateDeMiseEnCirculation(?\DateTimeInterface $dateDeMiseEnCirculation): self
    {
        $this->dateDeMiseEnCirculation = $dateDeMiseEnCirculation;

        return $this;
    }

    public function getDateAchat(): ?\DateTimeInterface
    {
        return $this->dateAchat;
    }

    public function setDateAchat(?\DateTimeInterface $dateAchat): self
    {
        $this->dateAchat = $dateAchat;

        return $this;
    }

    public function getDateDernierEvenement(): ?\DateTimeInterface
    {
        return $this->dateDernierEvenement;
    }

    public function setDateDernierEvenement(?\DateTimeInterface $dateDernierEvenement): self
    {
        $this->dateDernierEvenement = $dateDernierEvenement;

        return $this;
    }

    public function getLibelleMarque(): ?string
    {
        return $this->libelleMarque;
    }

    public function setLibelleMarque(?string $libelleMarque): self
    {
        $this->libelleMarque = $libelleMarque;

        return $this;
    }

    public function getLibelleModele(): ?string
    {
        return $this->libelleModele;
    }

    public function setLibelleModele(?string $libelleModele): self
    {
        $this->libelleModele = $libelleModele;

        return $this;
    }

    public function getVersion(): ?string
    {
        return $this->version;
    }

    public function setVersion(?string $version): self
    {
        $this->version = $version;

        return $this;
    }

    public function getVin(): ?string
    {
        return $this->vin;
    }

    public function setVin(string $vin): self
    {
        $this->vin = $vin;

        return $this;
    }

    public function getImmatriculation(): ?string
    {
        return $this->immatriculation;
    }

    public function setImmatriculation(?string $immatriculation): self
    {
        $this->immatriculation = $immatriculation;

        return $this;
    }

    public function getTypeDeProspect(): ?string
    {
        return $this->typeDeProspect;
    }

    public function setTypeDeProspect(?string $typeDeProspect): self
    {
        $this->typeDeProspect = $typeDeProspect;

        return $this;
    }

    public function getKilometrage(): ?int
    {
        return $this->kilometrage;
    }

    public function setKilometrage(?int $kilometrage): self
    {
        $this->kilometrage = $kilometrage;

        return $this;
    }

    public function getLibelleEnergie(): ?string
    {
        return $this->libelleEnergie;
    }

    public function setLibelleEnergie(?string $libelleEnergie): self
    {
        $this->libelleEnergie = $libelleEnergie;

        return $this;
    }

    public function getVendeurVn(): ?string
    {
        return $this->vendeurVn;
    }

    public function setVendeurVn(?string $vendeurVn): self
    {
        $this->vendeurVn = $vendeurVn;

        return $this;
    }

    public function getVendeurVo(): ?string
    {
        return $this->vendeurVo;
    }

    public function setVendeurVo(?string $vendeurVo): self
    {
        $this->vendeurVo = $vendeurVo;

        return $this;
    }

    public function getCommentaireDeFacture(): ?string
    {
        return $this->commentaireDeFacture;
    }

    public function setCommentaireDeFacture(?string $commentaireDeFacture): self
    {
        $this->commentaireDeFacture = $commentaireDeFacture;

        return $this;
    }

    public function getTypeVnVo(): ?string
    {
        return $this->typeVnVo;
    }

    public function setTypeVnVo(?string $typeVnVo): self
    {
        $this->typeVnVo = $typeVnVo;

        return $this;
    }

    public function getNumeroDeDossierVn(): ?string
    {
        return $this->numeroDeDossierVn;
    }

    public function setNumeroDeDossierVn(?string $numeroDeDossierVn): self
    {
        $this->numeroDeDossierVn = $numeroDeDossierVn;

        return $this;
    }

    public function getIntermediaireDeVente(): ?string
    {
        return $this->intermediaireDeVente;
    }

    public function setIntermediaireDeVente(?string $intermediaireDeVente): self
    {
        $this->intermediaireDeVente = $intermediaireDeVente;

        return $this;
    }

    public function getDateEvenement(): ?\DateTimeInterface
    {
        return $this->dateEvenement;
    }

    public function setDateEvenement(?\DateTimeInterface $dateEvenement): self
    {
        $this->dateEvenement = $dateEvenement;

        return $this;
    }

    public function getOrigineEvenement(): ?string
    {
        return $this->origineEvenement;
    }

    public function setOrigineEvenement(?string $origineEvenement): self
    {
        $this->origineEvenement = $origineEvenement;

        return $this;
    }

    public function getProprietaire(): ?Proprietaire
    {
        return $this->proprietaire;
    }

    public function setProprietaire(?Proprietaire $proprietaire): self
    {
        $this->proprietaire = $proprietaire;

        return $this;
    }

}
